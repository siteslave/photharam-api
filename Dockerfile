FROM keymetrics/pm2:latest-alpine

WORKDIR /home/photharam

COPY . .

RUN npm install 

CMD [ "pm2-runtime", "start", "pm2.json" ]

EXPOSE 3000